package class3.stackoverflow

/**
  * Created by mark on 05/03/2017.
  */
object Exercises {

  //以尾遞迴實現平方和函數1+4+...+n^2
  def tailSumOfSquares(n:Int,acc:Int):Int= if (n==1) acc else tailSumOfSquares(n-1,n*n+acc)

  //用尾遞迴求算0*1+1*2+2*3+3*4+…+(n-1)*n之和.
  def tailSum(n:Int,acc:Int):Int= if (n==1) acc else tailSum(n-1, n*(n-1)+acc)





}
